#!/bin/bash

# Ensure enough space
rm -rd models/*

# Argument Evidence Model
bash scripts/train/argument_probability_regression_evidences_sentences/train.sh True data/processed/evidences_sentences_cross_topic.csv -motion-cross_topic 200
bash scripts/predict/argument_probability_regression_evidences_sentences/arg_rank_30k_strength_prob_corr.sh 200
bash scripts/predict/argument_probability_regression_evidences_sentences/mapped_ukp_is_arg_corr.sh 200
bash scripts/train/argument_probability_regression_evidences_sentences/train.sh True data/processed/evidences_sentences_cross_topic.csv -motion-cross_topic 201
bash scripts/predict/argument_probability_regression_evidences_sentences/arg_rank_30k_strength_prob_corr.sh 201
bash scripts/predict/argument_probability_regression_evidences_sentences/mapped_ukp_is_arg_corr.sh 201
bash scripts/train/argument_probability_regression_evidences_sentences/train.sh True data/processed/evidences_sentences_cross_topic.csv -motion-cross_topic 202
bash scripts/predict/argument_probability_regression_evidences_sentences/arg_rank_30k_strength_prob_corr.sh 202
bash scripts/predict/argument_probability_regression_evidences_sentences/mapped_ukp_is_arg_corr.sh 202

# Ensure enough space
rm -rd models/*

# Argument Quality Model
bash scripts/train/strength_regression_arg_rank_30k/weighted_average.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv -motion-cross_topic 200
bash scripts/predict/strength_regression_arg_rank_30k/evidences_sentences_arg_prob_corr.sh 200
bash scripts/predict/strength_regression_arg_rank_30k/mapped_ukp_is_arg_corr.sh 200
bash scripts/train/strength_regression_arg_rank_30k/weighted_average.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv -motion-cross_topic 201
bash scripts/predict/strength_regression_arg_rank_30k/evidences_sentences_arg_prob_corr.sh 201
bash scripts/predict/strength_regression_arg_rank_30k/mapped_ukp_is_arg_corr.sh 201
bash scripts/train/strength_regression_arg_rank_30k/weighted_average.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv -motion-cross_topic 202
bash scripts/predict/strength_regression_arg_rank_30k/evidences_sentences_arg_prob_corr.sh 202
bash scripts/predict/strength_regression_arg_rank_30k/mapped_ukp_is_arg_corr.sh 202

# Ensure enough space
rm -rd models/*

# Argument Detection Model
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 200 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 200
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 200
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 201 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 201
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 201
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 202 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 202
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 202
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 203 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 203
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 203
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 204 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 204
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 204
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 205 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 205
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 205
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 206 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 206
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 206
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 207 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 207
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 207
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 208 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 208
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 208
bash scripts/train/argument_detection_ukp/train.sh True data/processed/ukp_sentential_argument_mining_cross_topic.csv -motion-cross_topic 209 2e-5
bash scripts/predict/argument_detection_ukp/regression_mapped_arg_rank_30k_strength_prob_corr.sh 209
bash scripts/predict/argument_detection_ukp/regression_mapped_evidences_sentences_arg_prob_corr.sh 209

# Multi-Task AQ/AId
bash scripts/train/multitask/train_strength_detection.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv 18.2 1 -motion-strength_detection_wa_cross_topic 200
bash scripts/train/multitask/train_strength_detection.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv 18.2 1 -motion-strength_detection_wa_cross_topic 201
bash scripts/train/multitask/train_strength_detection.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv 18.2 1 -motion-strength_detection_wa_cross_topic 202

# Multi-Task AQ/ED
bash scripts/train/multitask/train_strength_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/evidences_sentences_cross_topic.csv 1.8 1 -motion-strength_evidence_wa_cross_topic 200
bash scripts/train/multitask/train_strength_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/evidences_sentences_cross_topic.csv 1.8 1 -motion-strength_evidence_wa_cross_topic 201
bash scripts/train/multitask/train_strength_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/evidences_sentences_cross_topic.csv 1.8 1 -motion-strength_evidence_wa_cross_topic 202

# Multi-Task AQ/AId/ED
bash scripts/train/multitask/train_strength_detection_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv data/processed/evidences_sentences_cross_topic.csv 18.2 1 10 -motion-strength_detection_evidence_wa_cross_topic 200
bash scripts/train/multitask/train_strength_detection_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv data/processed/evidences_sentences_cross_topic.csv 18.2 1 10 -motion-strength_detection_evidence_wa_cross_topic 201
bash scripts/train/multitask/train_strength_detection_evidence.sh True data/processed/arg_quality_rank_30k_cross_topic_wa.csv data/processed/ukp_sentential_argument_mining_cross_topic.csv data/processed/evidences_sentences_cross_topic.csv 18.2 1 10 -motion-strength_detection_evidence_wa_cross_topic 202

# Remove failed predictions (no motion/in-topic settings)
grep -lrIZ "# Exception" . | xargs -0 rm -f --
# Remove empty dirs
find . -type d -empty -print -delete

# Average
python3 src/results/average_seeds.py results/200 results/201 results/202 results/203 results/204 results/205 results/206 results/207 results/208 results/209 results/average
