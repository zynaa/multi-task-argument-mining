# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 203 \
    --result_prefix 203
```

# Results
```json
{
    "test_loss": 0.5657894611358643,
    "test_f1_micro": 0.7495957962813259,
    "test_f1_macro": 0.7317867256791648,
    "test_f1_weighted": 0.763661470082405,
    "test_precision_micro": 0.7495957962813258,
    "test_precision_macro": 0.7378034712390541,
    "test_precision_weighted": 0.838214238949741,
    "test_recall_micro": 0.7495957962813258,
    "test_recall_macro": 0.8011640877024235,
    "test_recall_weighted": 0.7495957962813258,
    "test_f1_true": 0.8008998875140607,
    "test_f1_false": 0.662673563844269,
    "test_precision_true": 0.9555214723926381,
    "test_precision_false": 0.5200854700854701,
    "test_recall_true": 0.6893499308437068,
    "test_recall_false": 0.9129782445611403,
    "epochs_trained_total": 3.4744525547445253,
    "epochs_trained_best": 1.4306569343065694
}
```

