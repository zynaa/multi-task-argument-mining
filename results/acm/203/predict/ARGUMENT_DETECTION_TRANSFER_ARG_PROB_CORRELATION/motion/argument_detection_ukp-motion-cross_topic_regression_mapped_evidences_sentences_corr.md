# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 203 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-392
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07233183830976486,
            "root_mean_squared_error": 0.2689457833766937,
            "spearman": 0.5666633133810232,
            "pearson": 0.567612069637576
        },
        "positive_output": {
            "mean_squared_error": 0.07120971381664276,
            "root_mean_squared_error": 0.2668514847755432,
            "spearman": 0.5655990195654843,
            "pearson": 0.5606804770357536
        },
        "negative_output": {
            "mean_squared_error": 0.07001310586929321,
            "root_mean_squared_error": 0.26459988951683044,
            "spearman": 0.5711933636445097,
            "pearson": 0.569553386581779
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.06978575885295868,
            "root_mean_squared_error": 0.26416993141174316,
            "spearman": 0.568502539861278,
            "pearson": 0.5720131689274929
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07456864416599274,
            "root_mean_squared_error": 0.27307260036468506,
            "spearman": 0.5685025677936162,
            "pearson": 0.571465078090361
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.4295005798339844,
            "root_mean_squared_error": 1.1956171989440918,
            "spearman": 0.5655990096214374,
            "pearson": 0.5567839437316247
        },
        "negative_output": {
            "mean_squared_error": 1.6146938800811768,
            "root_mean_squared_error": 1.270706057548523,
            "spearman": -0.5711930155075471,
            "pearson": -0.5598439304112897
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.0820411667227745,
            "root_mean_squared_error": 0.286428302526474,
            "spearman": 0.5685025147369919,
            "pearson": 0.5719445654316913
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.47183167934417725,
            "root_mean_squared_error": 0.6869000792503357,
            "spearman": -0.5685025012342961,
            "pearson": -0.571944565123573
        }
    }
}
```

