# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 203 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-392
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.036202605813741684,
            "root_mean_squared_error": 0.1902698278427124,
            "spearman": 0.25318034750874713,
            "pearson": 0.2934536250598919
        },
        "positive_output": {
            "mean_squared_error": 0.03640925511717796,
            "root_mean_squared_error": 0.1908120959997177,
            "spearman": 0.24664459894039234,
            "pearson": 0.2835789248201852
        },
        "negative_output": {
            "mean_squared_error": 0.03617757558822632,
            "root_mean_squared_error": 0.19020403921604156,
            "spearman": 0.2598115526452216,
            "pearson": 0.2935188339671841
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.0362393744289875,
            "root_mean_squared_error": 0.19036641716957092,
            "spearman": 0.2537124016770571,
            "pearson": 0.29212376215269076
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03622935712337494,
            "root_mean_squared_error": 0.1903401017189026,
            "spearman": 0.25371241370440684,
            "pearson": 0.29211833836415946
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.5213308930397034,
            "root_mean_squared_error": 0.7220324873924255,
            "spearman": 0.24664471637854005,
            "pearson": 0.28399704211084215
        },
        "negative_output": {
            "mean_squared_error": 1.551166296005249,
            "root_mean_squared_error": 1.2454582452774048,
            "spearman": -0.2598114312775483,
            "pearson": -0.29493207382931586
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07036559283733368,
            "root_mean_squared_error": 0.2652651369571686,
            "spearman": 0.25371248285576287,
            "pearson": 0.2922812215760545
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.37807098031044006,
            "root_mean_squared_error": 0.6148747801780701,
            "spearman": -0.2537124076139358,
            "pearson": -0.2922812203185759
        }
    }
}
```

