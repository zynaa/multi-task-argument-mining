# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 207 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07334743440151215,
            "root_mean_squared_error": 0.2708273231983185,
            "spearman": 0.5445288921656434,
            "pearson": 0.5325647535839222
        },
        "positive_output": {
            "mean_squared_error": 0.07688992470502853,
            "root_mean_squared_error": 0.27729031443595886,
            "spearman": 0.5317281310190517,
            "pearson": 0.5316339579384532
        },
        "negative_output": {
            "mean_squared_error": 0.07394587248563766,
            "root_mean_squared_error": 0.27192988991737366,
            "spearman": 0.5461638010822327,
            "pearson": 0.5259792868630246
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07414011657238007,
            "root_mean_squared_error": 0.2722868323326111,
            "spearman": 0.5438974240843736,
            "pearson": 0.5346102240800108
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07724934816360474,
            "root_mean_squared_error": 0.2779376804828644,
            "spearman": 0.543897350760215,
            "pearson": 0.5358902245901169
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.5460548400878906,
            "root_mean_squared_error": 1.243404507637024,
            "spearman": 0.5317280620755964,
            "pearson": 0.5278778959607332
        },
        "negative_output": {
            "mean_squared_error": 2.0686757564544678,
            "root_mean_squared_error": 1.4382891654968262,
            "spearman": -0.5461637838395031,
            "pearson": -0.524772391302877
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.10677911341190338,
            "root_mean_squared_error": 0.32677072286605835,
            "spearman": 0.5438973863102384,
            "pearson": 0.5355782307144283
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4204619228839874,
            "root_mean_squared_error": 0.648430347442627,
            "spearman": -0.5438973863310281,
            "pearson": -0.535578231402619
        }
    }
}
```

