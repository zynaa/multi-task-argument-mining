# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 207 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03519797325134277,
            "root_mean_squared_error": 0.18761122226715088,
            "spearman": 0.2680676781136398,
            "pearson": 0.33275165502619136
        },
        "positive_output": {
            "mean_squared_error": 0.035374801605939865,
            "root_mean_squared_error": 0.18808190524578094,
            "spearman": 0.26878294460308777,
            "pearson": 0.32605363224305817
        },
        "negative_output": {
            "mean_squared_error": 0.03534267842769623,
            "root_mean_squared_error": 0.1879964917898178,
            "spearman": 0.2647627739622496,
            "pearson": 0.3272328039874125
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.03519796207547188,
            "root_mean_squared_error": 0.1876111924648285,
            "spearman": 0.26757065531722357,
            "pearson": 0.3338884702145103
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03520404174923897,
            "root_mean_squared_error": 0.1876274049282074,
            "spearman": 0.26757131021993924,
            "pearson": 0.33341813658693015
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.7577980160713196,
            "root_mean_squared_error": 0.8705159425735474,
            "spearman": 0.26878354683051375,
            "pearson": 0.3272543358894236
        },
        "negative_output": {
            "mean_squared_error": 5.573742866516113,
            "root_mean_squared_error": 2.360877513885498,
            "spearman": -0.26476243395626897,
            "pearson": -0.32704838551895954
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.051000453531742096,
            "root_mean_squared_error": 0.22583280503749847,
            "spearman": 0.26757121741109624,
            "pearson": 0.33370906228076913
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4195065200328827,
            "root_mean_squared_error": 0.6476932168006897,
            "spearman": -0.2675711052039617,
            "pearson": -0.33370906237198417
        }
    }
}
```

