# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 207 \
    --result_prefix 207
```

# Results
```json
{
    "test_loss": 0.5383135676383972,
    "test_f1_micro": 0.7558609539207761,
    "test_f1_macro": 0.7407656515563904,
    "test_f1_weighted": 0.7675680309638423,
    "test_precision_micro": 0.7558609539207761,
    "test_precision_macro": 0.74497633841959,
    "test_precision_weighted": 0.8310785207424054,
    "test_recall_micro": 0.7558609539207761,
    "test_recall_macro": 0.7991793600897805,
    "test_recall_weighted": 0.7558609539207761,
    "test_f1_true": 0.8033213936828394,
    "test_f1_false": 0.6782099094299414,
    "test_precision_true": 0.9459355828220859,
    "test_precision_false": 0.544017094017094,
    "test_recall_true": 0.6980758347481607,
    "test_recall_false": 0.9002828854314003,
    "epochs_trained_total": 2.9635036496350367,
    "epochs_trained_best": 0.9197080291970803
}
```

