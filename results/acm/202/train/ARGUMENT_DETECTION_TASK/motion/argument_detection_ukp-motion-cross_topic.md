# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 202 \
    --result_prefix 202
```

# Results
```json
{
    "test_loss": 0.4951224625110626,
    "test_f1_micro": 0.7687954729183508,
    "test_f1_macro": 0.7558345940215772,
    "test_f1_weighted": 0.7787094083551813,
    "test_precision_micro": 0.7687954729183508,
    "test_precision_macro": 0.7585417649834827,
    "test_precision_weighted": 0.8355210947034624,
    "test_recall_micro": 0.7687954729183508,
    "test_recall_macro": 0.8088508879075449,
    "test_recall_weighted": 0.7687954729183508,
    "test_f1_true": 0.8120893561103811,
    "test_f1_false": 0.6995798319327731,
    "test_precision_true": 0.9478527607361963,
    "test_precision_false": 0.5692307692307692,
    "test_recall_true": 0.7103448275862069,
    "test_recall_false": 0.9073569482288828,
    "epochs_trained_total": 2.9635036496350367,
    "epochs_trained_best": 0.9197080291970803
}
```

