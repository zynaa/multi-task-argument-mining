# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 202 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07398106157779694,
            "root_mean_squared_error": 0.27199459075927734,
            "spearman": 0.5525316826816601,
            "pearson": 0.5498857655888935
        },
        "positive_output": {
            "mean_squared_error": 0.07221657037734985,
            "root_mean_squared_error": 0.26873141527175903,
            "spearman": 0.5527495861645217,
            "pearson": 0.5533643298733228
        },
        "negative_output": {
            "mean_squared_error": 0.08006663620471954,
            "root_mean_squared_error": 0.2829604744911194,
            "spearman": 0.5358335873084858,
            "pearson": 0.4810086850846768
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07275672256946564,
            "root_mean_squared_error": 0.26973453164100647,
            "spearman": 0.552108556984405,
            "pearson": 0.5512527908322717
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07637844234704971,
            "root_mean_squared_error": 0.2763665020465851,
            "spearman": 0.5521084399322764,
            "pearson": 0.5539699279928592
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.980765700340271,
            "root_mean_squared_error": 1.4073967933654785,
            "spearman": 0.5527499168110093,
            "pearson": 0.5507692209146063
        },
        "negative_output": {
            "mean_squared_error": 0.6333786845207214,
            "root_mean_squared_error": 0.795850932598114,
            "spearman": -0.5358336929032199,
            "pearson": -0.47504240278528514
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.10381626337766647,
            "root_mean_squared_error": 0.3222053050994873,
            "spearman": 0.5521085889715993,
            "pearson": 0.5529832307921413
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.3986980617046356,
            "root_mean_squared_error": 0.6314254403114319,
            "spearman": -0.5521086082893667,
            "pearson": -0.552983231980917
        }
    }
}
```

