# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 202 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.0351719968020916,
            "root_mean_squared_error": 0.18754199147224426,
            "spearman": 0.2847046205985841,
            "pearson": 0.3342927339971498
        },
        "positive_output": {
            "mean_squared_error": 0.03520622476935387,
            "root_mean_squared_error": 0.187633216381073,
            "spearman": 0.28436040981429306,
            "pearson": 0.3328150931533028
        },
        "negative_output": {
            "mean_squared_error": 0.03547199070453644,
            "root_mean_squared_error": 0.18834009766578674,
            "spearman": 0.2837064265479367,
            "pearson": 0.32427015427201406
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.035171765834093094,
            "root_mean_squared_error": 0.18754136562347412,
            "spearman": 0.28631032443422144,
            "pearson": 0.33571434189068416
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.035169534385204315,
            "root_mean_squared_error": 0.18753542006015778,
            "spearman": 0.28630963442350144,
            "pearson": 0.3351804267149837
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.5738168954849243,
            "root_mean_squared_error": 0.7575070261955261,
            "spearman": 0.2843600591632817,
            "pearson": 0.33310075523454963
        },
        "negative_output": {
            "mean_squared_error": 1.8636142015457153,
            "root_mean_squared_error": 1.365142583847046,
            "spearman": -0.2837062255740881,
            "pearson": -0.32331609819756707
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.05104312673211098,
            "root_mean_squared_error": 0.22592726349830627,
            "spearman": 0.286310459521132,
            "pearson": 0.3355222977480254
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.3896639049053192,
            "root_mean_squared_error": 0.6242306232452393,
            "spearman": -0.28631027702702855,
            "pearson": -0.3355222987130972
        }
    }
}
```

