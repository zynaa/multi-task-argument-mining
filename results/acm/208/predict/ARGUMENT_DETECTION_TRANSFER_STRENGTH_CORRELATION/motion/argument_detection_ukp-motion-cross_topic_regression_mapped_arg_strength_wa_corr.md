# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 208 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-280
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03617524355649948,
            "root_mean_squared_error": 0.1901978999376297,
            "spearman": 0.2548665847149039,
            "pearson": 0.29394675717679447
        },
        "positive_output": {
            "mean_squared_error": 0.03647491708397865,
            "root_mean_squared_error": 0.1909840703010559,
            "spearman": 0.2557015059659132,
            "pearson": 0.2801253672069464
        },
        "negative_output": {
            "mean_squared_error": 0.036490701138973236,
            "root_mean_squared_error": 0.19102539122104645,
            "spearman": 0.25210216828934495,
            "pearson": 0.2951527239105786
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.036192744970321655,
            "root_mean_squared_error": 0.1902439147233963,
            "spearman": 0.2544089652097896,
            "pearson": 0.2947505656708253
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03619877249002457,
            "root_mean_squared_error": 0.19025975465774536,
            "spearman": 0.25440867898059855,
            "pearson": 0.2938487648921971
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.730320155620575,
            "root_mean_squared_error": 0.854587733745575,
            "spearman": 0.25570138670815173,
            "pearson": 0.2778063392819333
        },
        "negative_output": {
            "mean_squared_error": 5.014075756072998,
            "root_mean_squared_error": 2.239213228225708,
            "spearman": -0.25210184676991815,
            "pearson": -0.2953631962570557
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.06931019574403763,
            "root_mean_squared_error": 0.26326829195022583,
            "spearman": 0.2544081420085011,
            "pearson": 0.29437083199577346
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.3999559283256531,
            "root_mean_squared_error": 0.6324207186698914,
            "spearman": -0.25440812868992135,
            "pearson": -0.2943708322753968
        }
    }
}
```

