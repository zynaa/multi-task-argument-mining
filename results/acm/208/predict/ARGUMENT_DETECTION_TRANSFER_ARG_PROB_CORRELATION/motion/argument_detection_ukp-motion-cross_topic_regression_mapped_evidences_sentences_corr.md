# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 208 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-280
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.06970187276601791,
            "root_mean_squared_error": 0.26401111483573914,
            "spearman": 0.5698755542855056,
            "pearson": 0.568404189528155
        },
        "positive_output": {
            "mean_squared_error": 0.07280806452035904,
            "root_mean_squared_error": 0.2698296904563904,
            "spearman": 0.5605150450897538,
            "pearson": 0.5574801334944965
        },
        "negative_output": {
            "mean_squared_error": 0.0701940655708313,
            "root_mean_squared_error": 0.26494163274765015,
            "spearman": 0.5707416162345224,
            "pearson": 0.5620170036767274
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07098980993032455,
            "root_mean_squared_error": 0.26643913984298706,
            "spearman": 0.5697948956502634,
            "pearson": 0.5634266136456264
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07293722778558731,
            "root_mean_squared_error": 0.2700689435005188,
            "spearman": 0.5697950201102098,
            "pearson": 0.5636394420715475
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.1360974311828613,
            "root_mean_squared_error": 1.0658787488937378,
            "spearman": 0.5605151608574448,
            "pearson": 0.5585394423698229
        },
        "negative_output": {
            "mean_squared_error": 2.7643306255340576,
            "root_mean_squared_error": 1.6626275777816772,
            "spearman": -0.5707416162345224,
            "pearson": -0.5601699013721161
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.09636852890253067,
            "root_mean_squared_error": 0.3104328215122223,
            "spearman": 0.5697948252286498,
            "pearson": 0.5638168804388937
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4652833938598633,
            "root_mean_squared_error": 0.6821168661117554,
            "spearman": -0.5697947841771134,
            "pearson": -0.5638168796179952
        }
    }
}
```

