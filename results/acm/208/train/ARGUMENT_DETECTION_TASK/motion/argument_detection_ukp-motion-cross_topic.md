# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 208 \
    --result_prefix 208
```

# Results
```json
{
    "test_loss": 0.5898281335830688,
    "test_f1_micro": 0.7336297493936944,
    "test_f1_macro": 0.7115835989824547,
    "test_f1_weighted": 0.7513569171220302,
    "test_precision_micro": 0.7336297493936944,
    "test_precision_macro": 0.720769427402863,
    "test_precision_weighted": 0.8391995567811167,
    "test_recall_micro": 0.7336297493936944,
    "test_recall_macro": 0.7930224971291366,
    "test_recall_weighted": 0.7336297493936944,
    "test_f1_true": 0.791323622545915,
    "test_f1_false": 0.6318435754189944,
    "test_precision_true": 0.9582055214723927,
    "test_precision_false": 0.48333333333333334,
    "test_recall_true": 0.6739482200647249,
    "test_recall_false": 0.9120967741935484,
    "epochs_trained_total": 3.065693430656934,
    "epochs_trained_best": 1.0218978102189782
}
```

