# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 209 \
    --result_prefix 209
```

# Results
```json
{
    "test_loss": 0.5342546701431274,
    "test_f1_micro": 0.7647534357316087,
    "test_f1_macro": 0.7505164695883751,
    "test_f1_weighted": 0.7757623926308606,
    "test_precision_micro": 0.7647534357316087,
    "test_precision_macro": 0.7540267290650726,
    "test_precision_weighted": 0.8379191812033551,
    "test_recall_micro": 0.7647534357316087,
    "test_recall_macro": 0.8086696883464626,
    "test_recall_weighted": 0.7647534357316087,
    "test_f1_true": 0.8101141924959218,
    "test_f1_false": 0.6909187466808285,
    "test_precision_true": 0.9520705521472392,
    "test_precision_false": 0.555982905982906,
    "test_recall_true": 0.7049971607041454,
    "test_recall_false": 0.9123422159887798,
    "epochs_trained_total": 3.883211678832117,
    "epochs_trained_best": 1.8394160583941606
}
```

