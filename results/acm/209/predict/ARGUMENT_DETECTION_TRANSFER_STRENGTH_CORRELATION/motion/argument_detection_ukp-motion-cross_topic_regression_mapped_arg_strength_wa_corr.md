# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 209 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-504
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03534264862537384,
            "root_mean_squared_error": 0.18799640238285065,
            "spearman": 0.2791364512353841,
            "pearson": 0.3305043054624197
        },
        "positive_output": {
            "mean_squared_error": 0.03571318835020065,
            "root_mean_squared_error": 0.18897932767868042,
            "spearman": 0.2797666524968912,
            "pearson": 0.31691910070537604
        },
        "negative_output": {
            "mean_squared_error": 0.035643622279167175,
            "root_mean_squared_error": 0.18879517912864685,
            "spearman": 0.2726167013363623,
            "pearson": 0.32602445300625615
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.035421088337898254,
            "root_mean_squared_error": 0.18820491433143616,
            "spearman": 0.27846181538830506,
            "pearson": 0.32832468551311866
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03543105721473694,
            "root_mean_squared_error": 0.18823139369487762,
            "spearman": 0.27846179876754057,
            "pearson": 0.3272913504058947
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.788783609867096,
            "root_mean_squared_error": 0.8881348967552185,
            "spearman": 0.2797664914221185,
            "pearson": 0.31605789347770497
        },
        "negative_output": {
            "mean_squared_error": 5.8337297439575195,
            "root_mean_squared_error": 2.415311574935913,
            "spearman": -0.27261644581135563,
            "pearson": -0.32250695866976725
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.06413766741752625,
            "root_mean_squared_error": 0.25325414538383484,
            "spearman": 0.2784618229111851,
            "pearson": 0.3279152481652608
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.41397354006767273,
            "root_mean_squared_error": 0.6434077620506287,
            "spearman": -0.2784616241432558,
            "pearson": -0.32791524972075337
        }
    }
}
```

