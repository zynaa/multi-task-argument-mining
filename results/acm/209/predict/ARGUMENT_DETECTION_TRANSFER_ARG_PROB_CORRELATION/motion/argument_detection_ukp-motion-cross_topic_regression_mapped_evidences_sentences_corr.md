# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 209 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-504
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07068884372711182,
            "root_mean_squared_error": 0.2658737301826477,
            "spearman": 0.5561162839825008,
            "pearson": 0.5533476775936965
        },
        "positive_output": {
            "mean_squared_error": 0.07236041128635406,
            "root_mean_squared_error": 0.2689988911151886,
            "spearman": 0.5478665074228961,
            "pearson": 0.5469548329798961
        },
        "negative_output": {
            "mean_squared_error": 0.07130403071641922,
            "root_mean_squared_error": 0.26702815294265747,
            "spearman": 0.5522619529442129,
            "pearson": 0.546600778344866
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07181151211261749,
            "root_mean_squared_error": 0.26797670125961304,
            "spearman": 0.5568093174167703,
            "pearson": 0.5508315364925277
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07488996535539627,
            "root_mean_squared_error": 0.2736603021621704,
            "spearman": 0.5568091931406108,
            "pearson": 0.551020478249676
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.8423618078231812,
            "root_mean_squared_error": 1.3573362827301025,
            "spearman": 0.5478665074228961,
            "pearson": 0.5439884200620445
        },
        "negative_output": {
            "mean_squared_error": 2.46217679977417,
            "root_mean_squared_error": 1.5691324472427368,
            "spearman": -0.5522618483893137,
            "pearson": -0.5461855182988542
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.1016390398144722,
            "root_mean_squared_error": 0.31880879402160645,
            "spearman": 0.5568091062106704,
            "pearson": 0.5512168020603049
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4596380293369293,
            "root_mean_squared_error": 0.6779661178588867,
            "spearman": -0.5568090192523263,
            "pearson": -0.5512168014705541
        }
    }
}
```

