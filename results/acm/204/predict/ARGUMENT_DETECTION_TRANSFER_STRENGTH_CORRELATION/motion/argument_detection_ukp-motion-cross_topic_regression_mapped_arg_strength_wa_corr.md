# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 204 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03558048978447914,
            "root_mean_squared_error": 0.1886279135942459,
            "spearman": 0.28020147586674043,
            "pearson": 0.319429906221671
        },
        "positive_output": {
            "mean_squared_error": 0.03556481748819351,
            "root_mean_squared_error": 0.18858636915683746,
            "spearman": 0.28033513199047305,
            "pearson": 0.3199380531722725
        },
        "negative_output": {
            "mean_squared_error": 0.03585405275225639,
            "root_mean_squared_error": 0.1893516629934311,
            "spearman": 0.2681424303469507,
            "pearson": 0.30914070471481375
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.035702165216207504,
            "root_mean_squared_error": 0.18895016610622406,
            "spearman": 0.27666329520877686,
            "pearson": 0.3165827976496323
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03568040207028389,
            "root_mean_squared_error": 0.18889257311820984,
            "spearman": 0.27666279619879786,
            "pearson": 0.3160597949027433
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.47858771681785583,
            "root_mean_squared_error": 0.6918003559112549,
            "spearman": 0.2803350962170166,
            "pearson": 0.31915938602548727
        },
        "negative_output": {
            "mean_squared_error": 2.161388397216797,
            "root_mean_squared_error": 1.4701660871505737,
            "spearman": -0.2681424262200024,
            "pearson": -0.3084972426241251
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.06021031737327576,
            "root_mean_squared_error": 0.24537791311740875,
            "spearman": 0.27666228106562574,
            "pearson": 0.3164057382664977
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.36984729766845703,
            "root_mean_squared_error": 0.6081507205963135,
            "spearman": -0.276662106828715,
            "pearson": -0.3164057383903358
        }
    }
}
```

