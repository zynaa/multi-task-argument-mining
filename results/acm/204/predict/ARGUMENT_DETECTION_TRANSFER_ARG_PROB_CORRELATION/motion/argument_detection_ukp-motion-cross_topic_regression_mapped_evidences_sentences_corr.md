# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 204 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-252
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07344642281532288,
            "root_mean_squared_error": 0.27101001143455505,
            "spearman": 0.5674827762116248,
            "pearson": 0.5689252071163288
        },
        "positive_output": {
            "mean_squared_error": 0.07220319658517838,
            "root_mean_squared_error": 0.2687065303325653,
            "spearman": 0.5686080055406886,
            "pearson": 0.5725204043041154
        },
        "negative_output": {
            "mean_squared_error": 0.07340415567159653,
            "root_mean_squared_error": 0.27093201875686646,
            "spearman": 0.5617927636063185,
            "pearson": 0.5362615512774873
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07058414071798325,
            "root_mean_squared_error": 0.2656767666339874,
            "spearman": 0.5665325734013202,
            "pearson": 0.5692887117706557
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07516692578792572,
            "root_mean_squared_error": 0.2741658687591553,
            "spearman": 0.5665328393447231,
            "pearson": 0.5702000182807256
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.5548033714294434,
            "root_mean_squared_error": 1.2469176054000854,
            "spearman": 0.5686080055334438,
            "pearson": 0.571168384662063
        },
        "negative_output": {
            "mean_squared_error": 1.1497679948806763,
            "root_mean_squared_error": 1.0722723007202148,
            "spearman": -0.5617926674824252,
            "pearson": -0.5255978121907969
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.08659333735704422,
            "root_mean_squared_error": 0.2942674458026886,
            "spearman": 0.5665327005395943,
            "pearson": 0.5699926609836851
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4386284351348877,
            "root_mean_squared_error": 0.6622902750968933,
            "spearman": -0.5665326614704712,
            "pearson": -0.5699926587713778
        }
    }
}
```

