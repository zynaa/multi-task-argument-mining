# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 204 \
    --result_prefix 204
```

# Results
```json
{
    "test_loss": 0.5321850180625916,
    "test_f1_micro": 0.753839935327405,
    "test_f1_macro": 0.7368285084720005,
    "test_f1_weighted": 0.7672273062231526,
    "test_precision_micro": 0.753839935327405,
    "test_precision_macro": 0.7422467358817052,
    "test_precision_weighted": 0.8394911849635454,
    "test_recall_micro": 0.753839935327405,
    "test_recall_macro": 0.80435921190785,
    "test_recall_weighted": 0.753839935327405,
    "test_f1_true": 0.8037383177570093,
    "test_f1_false": 0.6699186991869919,
    "test_precision_true": 0.9562883435582822,
    "test_precision_false": 0.5282051282051282,
    "test_recall_true": 0.6931628682601445,
    "test_recall_false": 0.9155555555555556,
    "epochs_trained_total": 2.9635036496350367,
    "epochs_trained_best": 0.9197080291970803
}
```

