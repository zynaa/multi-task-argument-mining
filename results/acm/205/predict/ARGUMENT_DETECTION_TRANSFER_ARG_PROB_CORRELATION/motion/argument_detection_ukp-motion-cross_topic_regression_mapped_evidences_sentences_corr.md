# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 205 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-168
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07164316624403,
            "root_mean_squared_error": 0.2676624059677124,
            "spearman": 0.5683466414972862,
            "pearson": 0.5583360396716718
        },
        "positive_output": {
            "mean_squared_error": 0.07419656962156296,
            "root_mean_squared_error": 0.2723904848098755,
            "spearman": 0.5675648147568062,
            "pearson": 0.5379055463011729
        },
        "negative_output": {
            "mean_squared_error": 0.07303793728351593,
            "root_mean_squared_error": 0.27025532722473145,
            "spearman": 0.5679139770398293,
            "pearson": 0.5673463410581396
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07236868888139725,
            "root_mean_squared_error": 0.26901429891586304,
            "spearman": 0.568591066533622,
            "pearson": 0.5705128713834073
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.0715884193778038,
            "root_mean_squared_error": 0.26756012439727783,
            "spearman": 0.5685909190808134,
            "pearson": 0.5714483649342501
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.6331608295440674,
            "root_mean_squared_error": 0.7957140207290649,
            "spearman": 0.5675647227009241,
            "pearson": 0.537535950030103
        },
        "negative_output": {
            "mean_squared_error": 0.9780237078666687,
            "root_mean_squared_error": 0.988950788974762,
            "spearman": -0.567913862752725,
            "pearson": -0.5675632959028896
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.08097365498542786,
            "root_mean_squared_error": 0.2845587134361267,
            "spearman": 0.568591066533622,
            "pearson": 0.5711440412048885
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4040243327617645,
            "root_mean_squared_error": 0.6356290578842163,
            "spearman": -0.5685909853943897,
            "pearson": -0.5711440412692745
        }
    }
}
```

