# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 205 \
    --result_prefix 205
```

# Results
```json
{
    "test_loss": 0.5232533812522888,
    "test_f1_micro": 0.7394907033144706,
    "test_f1_macro": 0.7176211089994831,
    "test_f1_weighted": 0.7571039059980732,
    "test_precision_micro": 0.7394907033144705,
    "test_precision_macro": 0.726548817576425,
    "test_precision_weighted": 0.8465992949823251,
    "test_recall_micro": 0.7394907033144705,
    "test_recall_macro": 0.802158370863709,
    "test_recall_weighted": 0.7394907033144705,
    "test_f1_true": 0.7962055335968379,
    "test_f1_false": 0.6390366844021282,
    "test_precision_true": 0.9654907975460123,
    "test_precision_false": 0.4876068376068376,
    "test_recall_true": 0.6774280333602367,
    "test_recall_false": 0.9268887083671812,
    "epochs_trained_total": 2.656934306569343,
    "epochs_trained_best": 0.6131386861313869
}
```

