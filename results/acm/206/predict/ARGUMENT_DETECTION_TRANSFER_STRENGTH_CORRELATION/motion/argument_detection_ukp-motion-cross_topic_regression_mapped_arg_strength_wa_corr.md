# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 206 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-476
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03514218330383301,
            "root_mean_squared_error": 0.1874624788761139,
            "spearman": 0.29446813077385064,
            "pearson": 0.34018440198139266
        },
        "positive_output": {
            "mean_squared_error": 0.03520772233605385,
            "root_mean_squared_error": 0.18763720989227295,
            "spearman": 0.29512504768846587,
            "pearson": 0.33778997360123153
        },
        "negative_output": {
            "mean_squared_error": 0.035203974694013596,
            "root_mean_squared_error": 0.18762722611427307,
            "spearman": 0.2889501823246652,
            "pearson": 0.33696297216476273
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.035378649830818176,
            "root_mean_squared_error": 0.18809212744235992,
            "spearman": 0.29319314031015836,
            "pearson": 0.3325499769146272
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03531047701835632,
            "root_mean_squared_error": 0.18791082501411438,
            "spearman": 0.2931932866154803,
            "pearson": 0.33384242494631594
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.084944725036621,
            "root_mean_squared_error": 1.0416067838668823,
            "spearman": 0.2951244604074367,
            "pearson": 0.34064083825075014
        },
        "negative_output": {
            "mean_squared_error": 1.3854374885559082,
            "root_mean_squared_error": 1.1770460605621338,
            "spearman": -0.2889508830392594,
            "pearson": -0.33678125982575907
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.053298864513635635,
            "root_mean_squared_error": 0.2308654636144638,
            "spearman": 0.29319422362836206,
            "pearson": 0.3335229972125843
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4515882730484009,
            "root_mean_squared_error": 0.6720031499862671,
            "spearman": -0.29319365983453677,
            "pearson": -0.3335229980774589
        }
    }
}
```

