# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 206 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-476
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07357180118560791,
            "root_mean_squared_error": 0.2712412178516388,
            "spearman": 0.541613493170452,
            "pearson": 0.5303438428484907
        },
        "positive_output": {
            "mean_squared_error": 0.07595227658748627,
            "root_mean_squared_error": 0.27559441328048706,
            "spearman": 0.5417478573935607,
            "pearson": 0.5080457787750813
        },
        "negative_output": {
            "mean_squared_error": 0.07480373233556747,
            "root_mean_squared_error": 0.2735027074813843,
            "spearman": 0.536012019068947,
            "pearson": 0.5244230886300285
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07374000549316406,
            "root_mean_squared_error": 0.27155110239982605,
            "spearman": 0.539830535190271,
            "pearson": 0.5341671468118394
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07563841342926025,
            "root_mean_squared_error": 0.2750243842601776,
            "spearman": 0.5398303983525612,
            "pearson": 0.5383427417339387
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 2.0038187503814697,
            "root_mean_squared_error": 1.4155631065368652,
            "spearman": 0.5417476599791043,
            "pearson": 0.5101369620096927
        },
        "negative_output": {
            "mean_squared_error": 1.261877179145813,
            "root_mean_squared_error": 1.1233330965042114,
            "spearman": -0.536012012005489,
            "pearson": -0.5077546755154854
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.12826351821422577,
            "root_mean_squared_error": 0.3581389784812927,
            "spearman": 0.5398304732700542,
            "pearson": 0.5368003469313264
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.41384410858154297,
            "root_mean_squared_error": 0.6433071494102478,
            "spearman": -0.5398304707010801,
            "pearson": -0.5368003494345234
        }
    }
}
```

