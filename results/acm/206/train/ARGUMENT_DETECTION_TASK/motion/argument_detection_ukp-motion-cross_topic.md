# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 206 \
    --result_prefix 206
```

# Results
```json
{
    "test_loss": 0.46097511053085327,
    "test_f1_micro": 0.8090137429264348,
    "test_f1_macro": 0.8044363249908875,
    "test_f1_weighted": 0.8119706218834793,
    "test_precision_micro": 0.8090137429264349,
    "test_precision_macro": 0.803083215353154,
    "test_precision_weighted": 0.8306557428020644,
    "test_recall_micro": 0.8090137429264349,
    "test_recall_macro": 0.8226544849959364,
    "test_recall_weighted": 0.8090137429264349,
    "test_f1_true": 0.8343558282208589,
    "test_f1_false": 0.7745168217609162,
    "test_precision_true": 0.9125766871165644,
    "test_precision_false": 0.6935897435897436,
    "test_recall_true": 0.7684856312560543,
    "test_recall_false": 0.8768233387358185,
    "epochs_trained_total": 3.781021897810219,
    "epochs_trained_best": 1.7372262773722629
}
```

