# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 201 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-532
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07352706789970398,
            "root_mean_squared_error": 0.27115875482559204,
            "spearman": 0.5416383149918755,
            "pearson": 0.5653298355101267
        },
        "positive_output": {
            "mean_squared_error": 0.07105864584445953,
            "root_mean_squared_error": 0.26656827330589294,
            "spearman": 0.5407638220483213,
            "pearson": 0.5643578340425354
        },
        "negative_output": {
            "mean_squared_error": 0.07030603289604187,
            "root_mean_squared_error": 0.26515284180641174,
            "spearman": 0.5407381967399256,
            "pearson": 0.5596730122184101
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07049468904733658,
            "root_mean_squared_error": 0.26550835371017456,
            "spearman": 0.5414777074894661,
            "pearson": 0.5671573901988972
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07526987046003342,
            "root_mean_squared_error": 0.274353563785553,
            "spearman": 0.5414776278866539,
            "pearson": 0.566679203559743
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 2.2925913333892822,
            "root_mean_squared_error": 1.5141305923461914,
            "spearman": 0.5407638631147472,
            "pearson": 0.5551824709046402
        },
        "negative_output": {
            "mean_squared_error": 1.8007173538208008,
            "root_mean_squared_error": 1.3419080972671509,
            "spearman": -0.5407381967330359,
            "pearson": -0.5542576534741075
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.08143570274114609,
            "root_mean_squared_error": 0.2853694260120392,
            "spearman": 0.5414776267120206,
            "pearson": 0.5671294673945972
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.4890412986278534,
            "root_mean_squared_error": 0.6993148922920227,
            "spearman": -0.5414776761603751,
            "pearson": -0.5671294661644172
        }
    }
}
```

