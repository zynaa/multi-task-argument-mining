# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 201 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-532
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03630242124199867,
            "root_mean_squared_error": 0.19053193926811218,
            "spearman": 0.2596662060008889,
            "pearson": 0.28978894537066324
        },
        "positive_output": {
            "mean_squared_error": 0.03642333671450615,
            "root_mean_squared_error": 0.19084899127483368,
            "spearman": 0.26122714759765464,
            "pearson": 0.28513923329271523
        },
        "negative_output": {
            "mean_squared_error": 0.0364055298268795,
            "root_mean_squared_error": 0.19080233573913574,
            "spearman": 0.25539205418027644,
            "pearson": 0.2849273047060963
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.03637519106268883,
            "root_mean_squared_error": 0.19072280824184418,
            "spearman": 0.2581284934056383,
            "pearson": 0.2851376839272434
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.03641189634799957,
            "root_mean_squared_error": 0.19081901013851166,
            "spearman": 0.2581284452516409,
            "pearson": 0.2838873545762659
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 1.1654603481292725,
            "root_mean_squared_error": 1.0795649290084839,
            "spearman": 0.26122719584674114,
            "pearson": 0.2850792792538635
        },
        "negative_output": {
            "mean_squared_error": 3.6503803730010986,
            "root_mean_squared_error": 1.9105968475341797,
            "spearman": -0.2553918969221571,
            "pearson": -0.28268975904861876
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.10349263995885849,
            "root_mean_squared_error": 0.3217027187347412,
            "spearman": 0.25812853365519006,
            "pearson": 0.2845265060342834
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.3239448070526123,
            "root_mean_squared_error": 0.5691614747047424,
            "spearman": -0.25812844899791676,
            "pearson": -0.2845265027931728
        }
    }
}
```

