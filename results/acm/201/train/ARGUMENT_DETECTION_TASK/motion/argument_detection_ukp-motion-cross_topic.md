# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 201 \
    --result_prefix 201
```

# Results
```json
{
    "test_loss": 0.6637723445892334,
    "test_f1_micro": 0.7008892481810832,
    "test_f1_macro": 0.6643775596628303,
    "test_f1_weighted": 0.7314051447122701,
    "test_precision_micro": 0.7008892481810832,
    "test_precision_macro": 0.6850342142520056,
    "test_precision_weighted": 0.8622792950412463,
    "test_recall_micro": 0.7008892481810832,
    "test_recall_macro": 0.7912838640978654,
    "test_recall_weighted": 0.7008892481810832,
    "test_f1_true": 0.7750759878419453,
    "test_f1_false": 0.5536791314837153,
    "test_precision_true": 0.977760736196319,
    "test_precision_false": 0.3923076923076923,
    "test_recall_true": 0.6419939577039275,
    "test_recall_false": 0.9405737704918032,
    "epochs_trained_total": 3.985401459854015,
    "epochs_trained_best": 1.9416058394160585
}
```

