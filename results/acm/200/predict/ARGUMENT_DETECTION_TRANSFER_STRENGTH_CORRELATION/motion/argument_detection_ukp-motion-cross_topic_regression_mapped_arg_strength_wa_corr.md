# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/arg_quality_rank_30k_cross_topic_wa.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_STRENGTH_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_arg_strength_wa_corr \
    --result_prefix 200 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-476
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.03533707931637764,
            "root_mean_squared_error": 0.18798159062862396,
            "spearman": 0.27768647624429793,
            "pearson": 0.3327606776980797
        },
        "positive_output": {
            "mean_squared_error": 0.03543497249484062,
            "root_mean_squared_error": 0.18824179470539093,
            "spearman": 0.27597689196060526,
            "pearson": 0.3287897449640553
        },
        "negative_output": {
            "mean_squared_error": 0.03568989410996437,
            "root_mean_squared_error": 0.18891769647598267,
            "spearman": 0.27318000108074536,
            "pearson": 0.31860160447173214
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.0353129617869854,
            "root_mean_squared_error": 0.18791742622852325,
            "spearman": 0.2793714633567349,
            "pearson": 0.33538451273007897
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.035323143005371094,
            "root_mean_squared_error": 0.18794451653957367,
            "spearman": 0.2793712358952074,
            "pearson": 0.3345118857972261
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 0.7289630174636841,
            "root_mean_squared_error": 0.8537933230400085,
            "spearman": 0.27597657773809253,
            "pearson": 0.3234403405184815
        },
        "negative_output": {
            "mean_squared_error": 3.6057322025299072,
            "root_mean_squared_error": 1.8988765478134155,
            "spearman": -0.2731795408041933,
            "pearson": -0.31338138441274943
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.05487486720085144,
            "root_mean_squared_error": 0.23425385355949402,
            "spearman": 0.27937136998082324,
            "pearson": 0.3350301136906508
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.42850905656814575,
            "root_mean_squared_error": 0.65460604429245,
            "spearman": -0.27937143640345474,
            "pearson": -0.3350301171405628
        }
    }
}
```

