# Command
```bash
python3 src/models/predict_model.py \
    --input_dataset_csv data/processed/evidences_sentences_cross_topic.csv \
    --per_device_prediction_batch_size 64 \
    --task ARGUMENT_DETECTION_TRANSFER_ARG_PROB_CORRELATION \
    --include_motion True \
    --result_file_name argument_detection_ukp-motion-cross_topic_regression_mapped_evidences_sentences_corr \
    --result_prefix 200 \
    --model_name ./models/argument_detection_ukp-motion-cross_topic/checkpoint-476
```

# Results
```json
{
    "regression_metrics": {
        "all_outputs": {
            "mean_squared_error": 0.07174991816282272,
            "root_mean_squared_error": 0.2678617537021637,
            "spearman": 0.5692160997305683,
            "pearson": 0.5558212563549582
        },
        "positive_output": {
            "mean_squared_error": 0.07632407546043396,
            "root_mean_squared_error": 0.2762681245803833,
            "spearman": 0.5187337902403002,
            "pearson": 0.5236424900082659
        },
        "negative_output": {
            "mean_squared_error": 0.0723225474357605,
            "root_mean_squared_error": 0.26892852783203125,
            "spearman": 0.5697913376995914,
            "pearson": 0.5492639380244199
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.07366815209388733,
            "root_mean_squared_error": 0.2714187800884247,
            "spearman": 0.5452110905913061,
            "pearson": 0.5436257423064744
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.07698850333690643,
            "root_mean_squared_error": 0.2774680256843567,
            "spearman": 0.5452112950146419,
            "pearson": 0.5442341510336008
        }
    },
    "raw_metrics": {
        "positive_output": {
            "mean_squared_error": 2.3716022968292236,
            "root_mean_squared_error": 1.5400007963180542,
            "spearman": 0.5187337532584826,
            "pearson": 0.5202443368155126
        },
        "negative_output": {
            "mean_squared_error": 1.2004239559173584,
            "root_mean_squared_error": 1.095638632774353,
            "spearman": -0.5697913376995914,
            "pearson": -0.5441108169619026
        },
        "softmax_positive_output": {
            "mean_squared_error": 0.1036570817232132,
            "root_mean_squared_error": 0.32195818424224854,
            "spearman": 0.5452112950146419,
            "pearson": 0.5442113453630895
        },
        "softmax_negative_output": {
            "mean_squared_error": 0.43601080775260925,
            "root_mean_squared_error": 0.6603111624717712,
            "spearman": -0.5452112950146419,
            "pearson": -0.544211345851033
        }
    }
}
```

