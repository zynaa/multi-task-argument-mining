# Command
```bash
python3 src/models/train_model.py \
    --input_dataset_csv data/processed/ukp_sentential_argument_mining_cross_topic.csv \
    --task ARGUMENT_DETECTION_TASK \
    --model_name bert-large-uncased \
    --include_motion True \
    --per_device_train_batch_size 32 \
    --gradient_accumulation_steps 2 \
    --per_device_eval_batch_size 64 \
    --evaluation_strategy steps \
    --logging_epoch_step 0.1 \
    --learning_rate 2e-5 \
    --num_train_epochs 10 \
    --output_dir models/argument_detection_ukp-motion-cross_topic \
    --run_name argument_detection_ukp-motion-cross_topic \
    --load_best_model_at_end 1 \
    --save_total_limit 1 \
    --mlflow_env .mlflow \
    --early_stopping_patience 20 \
    --max_len_percentile 99 \
    --seed 200 \
    --result_prefix 200
```

# Results
```json
{
    "test_loss": 0.5281723141670227,
    "test_f1_micro": 0.753839935327405,
    "test_f1_macro": 0.7373442282065926,
    "test_f1_weighted": 0.7667704412317838,
    "test_precision_micro": 0.753839935327405,
    "test_precision_macro": 0.7424443526820828,
    "test_precision_weighted": 0.8365004303367577,
    "test_recall_micro": 0.753839935327405,
    "test_recall_macro": 0.8021108987552681,
    "test_recall_weighted": 0.753839935327405,
    "test_f1_true": 0.8031674208144797,
    "test_f1_false": 0.6715210355987056,
    "test_precision_true": 0.9528374233128835,
    "test_precision_false": 0.532051282051282,
    "test_recall_true": 0.6941340782122905,
    "test_recall_false": 0.9100877192982456,
    "epochs_trained_total": 3.781021897810219,
    "epochs_trained_best": 1.7372262773722629
}
```

